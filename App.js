import React from 'react';
import { StyleSheet, Text, View } from 'react-native';

import Header from './components';

export default function App() {
  return (
    <View style={styles.screen}>
      <Header title = "Welcome"></Header>
    </View>
  );
}

const styles = StyleSheet.create({
  screen: {
    flex: 1,
  }
});
